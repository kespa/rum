package;

import sys.FileSystem;
import haxe.Json;
import sys.io.File;
import Main.RumFile;

function getVariable(arg:String):Null<String> { // get the value of a defined variable
	var value = Main.variables.get(arg.substr(1));
	if (value != null) {
		return value;
	} else {
		Sys.println('No variable $arg has been defined');
		Sys.exit(1);
		return null;
	}
}

function parse():RumFile {
	var abspath = FileSystem.absolutePath('./.rum');
	if (!FileSystem.exists(abspath)) { // checks if file exists in the first place
		Sys.println("No .rum file found");
		Sys.exit(1);
	}

	var rawjson:RumFile = {defaultrum: [], tasks: {}, variables: {}};
	try { // catches error if json is incorrect
		rawjson = Json.parse(File.getContent(abspath));
	} catch (e) {
		Sys.println("Failed to parse json, maybe recreate .rum file?");
		Sys.exit(1);
	}
	return rawjson;
}
