#!/bin/bash

BINARY_LINK="https://codeberg.org/sharpcdf/rum/raw/branch/master/rum"

echo "What do you want to do?

.................................
1) Install Rum
2) Uninstall Rum
3) Reinstall/Update Rum
.................................

"

INPUT=0
INSTALL_DIR="$HOME/.local/bin"

installrum() {
    if [ -f "$INSTALL_DIR/rum" ]; then
        echo "Rum is already installed, if you wish to update it then choose the 3rd option"
        exit
    fi
    sudo touch "$INSTALL_DIR/rum"
    sudo curl -s "$BINARY_LINK" -o "$INSTALL_DIR/rum"
    sudo chmod +x $INSTALL_DIR/rum
    echo "Rum installed to $INSTALL_DIR"
}

uninstallrum() {
    if [ -f "$INSTALL_DIR/rum" ]; then
        sudo rm -rf "$INSTALL_DIR/rum"
        echo "Rum uninstalled"
    else
        echo "Rum is not installed in $INSTALL_DIR"
        exit
    fi
}

updaterum() {
    uninstallrum
    installrum
}

is_not_num() {
    printf %s "$1" | grep -vE '^[0-9]+$'
}

if ! [ -z $1 ]; then
    INSTALL_DIR=${1#*=} #if the install path is explicitly set
    if ! [ -d $INSTALL_DIR ]; then
        echo "Directory $INSTALL_DIR does not exist"
        exit
    fi
fi

while is_not_num "$INPUT" || [ "$INPUT" -lt 1 ] || [ "$INPUT" -gt 3 ]; do
    echo "Enter a number
    "
    read INPUT
    case $INPUT in
        1)  installrum
            exit; ;;
        2)  uninstallrum
            exit; ;;
        3)  updaterum
            exit; ;;
        *)  echo "Invalid input"
            INPUT=0
            continue; ;;
    esac
done