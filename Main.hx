package;

import parcl.Sharcl.HelpArgument;
import haxe.Timer;
import haxe.ValueException;
import parcl.Parcl;
import haxe.Json;
import sys.io.File;
import sys.FileSystem;
import Helpers;

typedef RumFile = {
	tasks:{},
	defaultrum:Array<String>,
	variables:{}
};

class Main {
	static var time:Float;
	static public var variables:Map<String, String> = [];
	static var json:RumFile;

	static function main() {
		var cli = new Parcl("Rum", "A small build tool", "Like run, but with a m");
		cli.addArg(HelpArgument.Normal("n", "RuN the .rum file in the current directory"));
		cli.addArg(HelpArgument.Normal("g", "Generate a new .rum file"));
		cli.addArg(HelpArgument.Normal("l", "List all defined tasks"));
		cli.addArg(HelpArgument.Option("t", "Specify a task to run"));
		cli.addArg(HelpArgument.Normal("v", "Prints the Version of Rum"));
		cli.addHelpFlags();
		cli.checkResults();

		for (item in cli.results) {
			switch (item) {
				case Normal(n):
					switch (n.name) {
						case "n":
							json = parse();
							time = Sys.time();

							// parse defined variables and put them in an array
							for (variable in Reflect.fields(json.variables)) { // ? maybe theres a more effective way then just spamming Reflect functions lol
								variables.set(variable, Reflect.field(json.variables, variable));
							}
							variables.set("operator", Sys.systemName().toLowerCase());

							if (cli.hasName("t")) { // checks if a specific task has been specified, calls it if true
								var taskname = cli.value("t");
								run(taskname);
							} else {
								run(); // if no task specified, the default task(the `defaultrum` array of strings) is run
							}
						case "g":
							generaterum(); // generates a new .rum file
						case "l":
							json = parse();
							var result = "\n";
							for (task in Reflect.fields(json.tasks)) { // loop through defined tasks and print them
								result += '$task\n';
							}
							Sys.println(result);
						case "v":
							Sys.println('Rum version ${haxe.macro.Compiler.getDefine("version")}');
							Sys.exit(0);
					}
				default:
					continue;
			}
		}
	}

	static function run(?task:String) {
		var cmds = new Array<String>();

		if (task != null) { // set cmds if task is defined
			if (Reflect.hasField(json.tasks, task)) {
				Sys.println('\nExecuting task $task\n');
				cmds = Reflect.field(json.tasks, task);
			} else {
				Sys.println('Task $task not defined');
				Sys.exit(1);
			}
		} else {
			Sys.println('\nExecuting default task\n');
			cmds = json.defaultrum;
		}

		for (cmd in cmds) { // splits the command and args
			if (StringTools.startsWith(cmd, "+")) { // if there is runtask syntax found
				var task2 = cmd.substr(1);
				if (Reflect.hasField(json.tasks, task2)) {
					run(task2);
				} else {
					Sys.println('Task $task2 is not defined');
					Sys.exit(1);
				}
			} else if (StringTools.startsWith(cmd, "%")) { // if there is runshellscript syntax found
				trace("yo this be a shell script");
				var script:String;
				var args = cmd.split(" ");
				script = args[0].substr(1); // get the name of the shellscript, without the %
				args = args.slice(1); // remove the actual shellscript name and leave just the args
				for (arg in args) {
					if (StringTools.startsWith(arg, "@")) { // checks and gets variables
						args[args.indexOf(arg)] = getVariable(arg); // replace the variable with the value
					}
				}
				if (FileSystem.exists(FileSystem.absolutePath('./$script'))) { // if the shell script exists in the current directory
					Sys.command("sh", [FileSystem.absolutePath('./$script'), args.join(" ")]);
				} else {
					Sys.println('Shell script $script does not exist in the current directory. Is it in a subdirectory? Try specifying with subdir/shellscript.sh');
					Sys.exit(1);
				}
			} else { // if its just a normal command
				var splitcmd = cmd.split(" ");
				for (arg in splitcmd) { // splits the commands into the args and actual program, then calls them
					if (StringTools.startsWith(arg, "@")) { // checks and gets variables
						splitcmd[splitcmd.indexOf(arg)] = getVariable(arg);
					}
				}
				// Sys.command(splitcmd[0], [splitcmd.slice(1).join(" ")]); // runs the current command with args
				Sys.command(splitcmd.join(" "));
			}
		}

		Sys.println('\nExecuted ' + (task != null ? 'task $task' : 'default task') + ' in ${(Sys.time() - time)} second(s)\n');
	}

	static public function generaterum() {
		var rumpath = FileSystem.absolutePath("./.rum");
		var file = File.write(rumpath);
		file.writeString(Json.stringify({
			tasks: {task1: ["echo this is a task, and @version is the current version!"]},
			defaultrum: ["echo this is the default task, run with 'rum n'!", "+task1"],
			variables: {version: "0.1.0"}
		}, "\t")); // writes the starting json to the new .rum file
		file.close();
		Sys.println("Generated .rum file");
	}
}
