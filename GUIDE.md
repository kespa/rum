# How to use Rum

Rum is a task executor and command runner to help manage projects. The layout of a .rum file is simple:
- The `tasks` object holds all user defined tasks and the underlying commands
- The `defaultrum` object holds the default commands that are run when no task is explicitly provided with `--t=taskname`
- The `variables` object holds all user defined variables and their values

Some convienant features are also provided to aid in building tasks:
- Executing external shell scripts within tasks with the syntax `%shellscript.sh`
- Executing other tasks within tasks for modularity with the syntax `+taskname`
- Using defined variables within command strings with the syntax `@variable`
Note that these features are available only within tasks.

Some global variables are also defined for the sake of conditional task execution:
- `operator`: The name of the operating system where the .rum file was executed on. Useful for cross-compilation. Can be either `windows`, `linux`, `bsd`, or `macos`.
- `arch`: The arch of the operating system where the .rum file was executed on.
- `name`: The name of the parent folder, useful for obtaining the name of the project where the .rum file lies.
Some more global variables may be added in the future.

Confused on how to implement conditional execution? You can use variables as arguments when calling shell scripts(`%shellscript.sh @operator`) and run specific code blocks only when the variable equals, lets say, `windows`!

Because .rum files are json, they can be parsed extremely easily to obtain tasks. This would be useful if, for instance, an application was only available through source code, and another application wished to clone the source code and build it through the definitions in the .rum file.