# Rum
## A simplistic task executor defined in a JSON file

This is untested on Windows, and may not work. This has only been tested on Linux.

Check GUIDE.md on how to use and make .rum files.

## Install and Manage Rum:
Run `sh <(curl -s https://codeberg.org/sharpcdf/rum/raw/branch/master/install.sh)` in a bash shell. Shells like fish do not support the `<()` syntax and/or command substitutions and the command will error.